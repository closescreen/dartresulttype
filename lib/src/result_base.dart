import 'dart:async';
/// Abstract class. Represent common type Result, which may be a Ok or Err.
/// Don't instantiate it. Use implementations: Ok() and Err() instead.
abstract class Result<T> {
  T? _val;
  T? get val => _val;

  Error? _err;
  Error? get err => _err;

  bool get isOk => this is Ok;
  bool get isErr => this is Err;

  /// Returns `result.val` if `result` is `Ok`.
  /// Throws contained `result.err` if `result` is `Err`.
  T get unwrap {
    if (isErr) {
      throw err!;
    }
    return val!;
  }

  @override
  String toString() => isErr ? 'Err($err)' : 'Ok($val)';

  /// Returns `val` if `result` is `Ok`.
  /// Returns `default_value` if `result` is `Err`.
  T unwrapOrDefault(T default_value) {
    if (isErr) {
      return default_value;
    }
    return val!;
  }

  /// Returns `result.val` if `result` is `Ok`.
  /// Returns call of `elseFunc(result.err)` if `result` is `Err`.
  T unwrapOrElse(T Function(Error? e) elseFunc) {
    if (isErr) {
      return elseFunc(err);
    }
    return val!;
  }

  /// If the `Result` is a `Err`, then replaces internal `err` with `newErr` value.
  /// And returns changed result;
  /// If the `Result` is a `Ok`, then return unchanged `Result` as is;
  Result<T> replacePossibleErrorWith(Error newErr) {
    if (isErr) {
      return Err(newErr);
    }
    return this;
  }

  /// If the Result is a Err, then replaces internal err with result if given function oldErrorReplacer.
  /// Function oldErrorReplacer should take old error and return new error;
  /// And returns changed result;
  /// If the Result is a Ok, then return unchanged Result as is;
  Result<T> replacePossibleError(
      Error Function(Error oldErr) oldErrorReplacer) {
    if (isErr) {
      return Err(oldErrorReplacer(_err!));
    }
    return this;
  }

  /// If `result` is `Err` then returns this `Err` as is.
  /// If `result` is `Ok` then returns result of call f(result.val).
  /// If result of call f() is null - returns Err('function returned null').
  /// If result of call f() is `Result` - returns it as is.
  /// If call of f() throws exception, then returns `Err`.
  Result<T2> pipe<T2>(Function(T) f) {
    if (isErr) {
      return Err<T2>(err);
    }
    var argument = unwrap;
    try {
      var res = f(argument);
      if (res == null) {
        return Err<T2>(
            StateError('Result of f($argument) == null, where `f` is $f'));
      } else {
        if (res is Error) {
          return Err<T2>(res);
        } else {
          if (res is Ok) {
            return Ok<T2>(res.unwrap);
          } else if (res is Err) {
            return Err<T2>(res.err!);
          } else {
            return Ok<T2>(res);
          }
        }
      }
    } catch (e) {
      return Err(StateError(e.toString()));
    }
  }

  /// If this result is Ok, then returns it as is.
  /// If this result is Err, then call `result = f()`.
  /// If result of call f() is null, then returns Err.
  /// If result of call f() is value , then return Ok(value).
  /// If call of f() throws exception, then returns Err.
  Result<T> or(T? Function() f) {
    if (isOk) {
      return this;
    }
    try {
      var res = f();
      if (res == null) {
        return Err<T>(StateError('Result of f() == null, where `f` is $f'));
      }
      return Ok<T>(res);
    } catch (e) {
      return Err(StateError('Raised exception $e while called $f'));
    }
  }

  /// If this result is Ok, then returns it as is.
  /// If this result is Err, then call `result = await f()`.
  /// If result of call f() is null, then returns Err.
  /// If result of call f() is value, then return Ok(value).
  /// If call of f() throws exception, then returns Err.
  Future<Result<T>> orAsync(Future<T> Function() f) async {
    if (isOk) {
      return this;
    }
    try {
      var res = await f();
      if (res == null) {
        return Err<T>(StateError('Result of f() == null, where `f` is $f'));
      }
      return Ok(res);
    } catch (e) {
      return Err(StateError('Raised exception $e while called $f'));
    }
  }

  /// If `result` is `Err` then returns this `Err` as is.
  /// If `result` is `Ok` then returns result of call f(result.val).
  /// Result of call f() must be a value of type T2.
  Result<T2> ok<T2>(T2 Function(T) f) {
    return isErr ? Err<T2>(err) : Ok(f(unwrap));
  }

  /// The same as ok(), but wrapped with Future<...>.
  /// If `result` is `Err` then returns this `Err` as is.
  /// If `result` is `Ok` then returns result of call f(result.val).
  /// Result of call f() must be a value of type T2.
  Future<Result<T2>> okAsync<T2>(Future<T2> Function(T) f) async {
    return isErr ? Err<T2>(err) : Ok(await f(unwrap));
  }

  /// If `result` is `Err` then returns this `Err` as is.
  /// If `result` is `Ok` then returns result of call `await f(result.val)`.
  /// If result of call f() is null - returns Err('function returned null').
  /// If result of call f() is `Result` - returns it as is.
  /// If call of f() throws exception, then returns `Err`.
  Future<Result<T2>> pipeAsync<T2>(Future Function(T) f) async {
    if (isErr) {
      return Err<T2>(err);
    }
    var argument = unwrap;
    var res = await f(argument);
    if (res == null) {
      return Err<T2>(
          StateError('Result of f($argument) == null, where `f` is $f'));
    } else {
      if (res is Error) {
        return Err<T2>(res);
      } else {
        if (res is Ok) {
          return Ok(res.unwrap);
        } else if (res is Err) {
          return Err<T2>(res.err);
        } else {
          return Ok(res);
        }
      }
    }
  }

  /// Test this result and return Ok or Err.
  Result<T> test(bool Function(Result<T>) testFunction, String failedTestMsg) =>
      testResult(testFunction, this, failedTestMsg);

  /// Calls f(result.val) with side effect if this result is Ok.
  /// Anyway returns this result as is.
  Result<T> onOk(Function(T) f) {
    if (isOk) {
      f(val!);
    }
    return this;
  }

  /// Calls f(result.err) with side effect if this result is Err.
  /// Anyway returns this result as is.
  Result<T> onErr(Function(Error) f) {
    if (isErr) {
      f(err!);
    }
    return this;
  }

  /// Calls f(this) with side effect and return this as is.
  Result<T> onResult(Function(Result<T>) f) {
    f(this);
    return this;
  }
}

/// Represent success result.
/// See isOk, val getters and unpack, for getting it's value.
/// Ok has Result as superclass.
/// Ok(123) is Result == true
class Ok<T> extends Result<T> {
  @override
  final T? _val;
  Ok(this._val) {
    ArgumentError.checkNotNull(val);
  }
}

/// Err has Result as superclass.
/// Err() is Result == true
/// See isErr, err getters for getting it's error.
class Err<T> extends Result<T> {
  @override
  final Error? _err;

  /// Create "failed" result.
  /// Err(StateError('Some error'));
  Err(this._err) {
    ArgumentError.checkNotNull(err);
  }
}

/// Calls `Function` f and awaits it.
/// Returns Ok(...) if all right
/// Returns Err(...) otherwise
Future<Result<T2>> tryAsResultAsync<T2>(FutureOr<T2?> Function() f) async {
  try {
    var callResult = await f();
    return callResult == null
        ? Err<T2>(StateError('async function returned null'))
        : Ok(callResult);
  } catch (e) {
    return Err(StateError('$e'));
  }
}

/// Calls `Function` f
/// Returns Err(...) if call result is null or function call throws error.
/// Otherwise seturns Ok(...).
Result<T> tryAsResultSync<T>(T? Function() f) {
  try {
    var callResult = f();
    return callResult == null
        ? Err<T>(StateError('function returned null'))
        : Ok(callResult);
  } catch (e) {
    return Err(StateError('$e'));
  }
}

/// Calls test(result) and returns result as is if test return true.
/// Otherwise returns Err(ArgumentError(failedTestMsg)).
Result<T> testResult<T>(bool Function(Result<T>) test, Result<T> result,
        String failedTestMsg) =>
    test(result) ? result : Err(ArgumentError(failedTestMsg));

extension ResultPipingAsync<T> on Future<Result<T>> {
  /// If `result` is `Err` then returns this `Err` as is.
  /// If `result` is `Ok` then returns result of call `f(result.val)`.
  /// Result of call f() must be a value of type T2.
  Future<Result<T2>> ok<T2>(T2 Function(T) f) async =>
      then((resultT) async => Ok(f(resultT.unwrap)));

  /// If `result` is `Err` then returns this `Err` as is.
  /// If `result` is `Ok` then returns result of call `await f(result.val)`.
  /// Result of call f() must be a value of type T2.
  Future<Result<T2>> okAsync<T2>(Future<T2> Function(T) f) async =>
      then((resultT) async => Ok(await f(resultT.unwrap)));

  /// If `result` is `Err` then returns this `Err` as is.
  /// If `result` is `Ok` then returns result of call f(result.val).
  /// If result of call f() is null - returns Err('function returned null').
  /// If result of call f() is `Result` - returns it as is.
  /// If call of f() throws exception, then returns `Err`.
  Future<Result<T2>> pipe<T2>(Function(T) f) async =>
      then((result) => result.pipe(f));

  /// If `result` is `Err` then returns this `Err` as is.
  /// If `result` is `Ok` then returns result of call `await f(result.val)`.
  /// If result of call f() is null - returns Err('function returned null').
  /// If result of call f() is `Result` - returns it as is.
  /// If call of f() throws exception, then returns `Err`.
  Future<Result<T2>> pipeAsync<T2>(Future Function(T) f) async {
    return then((resultT) async {
      if (resultT.isErr) {
        return Err<T2>(resultT.err);
      }
      var valueT = resultT.unwrap;
      var res = await f(valueT);
      if (res == null) {
        return Err<T2>(
            StateError('Result of f($valueT) == null, where `f` is $f'));
      } else {
        if (res is Error) {
          return Err<T2>(res);
        } else {
          if (res is Ok) {
            return Ok<T2>(res.val);
          } else if (res is Err) {
            return Err<T2>(res.err);
          } else {
            return Ok(res);
          }
        }
      }
    });
  }

  /// If this result is Ok, then returns it as is.
  /// If this result is Err, then call `result = await f()`.
  /// If result of call f() is null, then returns Err.
  /// If result of call f() is value, then return Ok(value).
  /// If call of f() throws exception, then returns Err.
  Future<Result<T>> orAsync(Future<T?> Function() f) async {
    return then((resultT) async {
      if (resultT.isOk) {
        return resultT;
      }
      try {
        var res = await f();
        if (res == null) {
          return Err<T>(StateError('null returned from $f'));
        }
        return Ok(res);
      } catch (e) {
        return Err<T>(StateError('Raised exception $e while called $f'));
      }
    });
  }

  /// If this result is Ok, then returns it as is.
  /// If this result is Err, then call `result = f()`.
  /// If result of call f() is null, then returns Err.
  /// If result of call f() is value , then return Ok(value).
  /// If call of f() throws exception, then returns Err.
  Future<Result<T>> or(T? Function() f) {
    return then((resultT) {
      if (resultT.isOk) {
        return resultT;
      }
      try {
        var res = f();
        if (res == null) {
          return Err<T>(StateError('null returned from $f'));
        }
        return Ok(res);
      } catch (e) {
        return Err<T>(StateError('Raised exception $e while called $f'));
      }
    });
  }

  /// Calls f(result.val) with side effect if this result is Ok.
  /// Anyway returns this result as is.
  Future<Result<T>> onOk(Function(T) f) {
    return then((resultT) {
      if (resultT.isOk) {
        f(resultT.val!);
      }
      return this;
    });
  }

  /// Calls f(result.err) with side effect if this result is Err.
  /// Anyway returns this result as is.
  Future<Result<T>> onErr(Function(Error) f) {
    return then((resultT) {
      if (resultT.isErr) {
        f(resultT.err!);
      }
      return this;
    });
  }

  /// Calls f(this) with side effect and return this as is.
  Future<Result<T>> onResult(Function(Result<T>) f) {
    return then((resultT) {
      f(resultT);
      return this;
    });
  }
}
