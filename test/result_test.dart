import 'dart:convert';
import 'dart:io';

import 'package:rust_like_result/rust_like_result.dart';
import 'package:test/test.dart';

void main() {
  late Result<String> good;
  late Err<String> bad;

  setUp(() {
    good = Ok('Good result');
    bad = Err(ArgumentError('Bad argument AAA'));
  });

  // good:
  test('good.isOk return true', () => expect(good.isOk, isTrue));
  test('good.isErr return false', () => expect(good.isErr, isFalse));
  test('ok is Ok type', () => expect(good, isA<Ok>()));
  test('ok is not Err type', () => expect(good, isNot(Err)));
  test('good val is String', () => expect(good.val, isA<String>()));
  test('good err is an null', () => expect(good.err, null));
  test('good can be unwraped to value',
      () => expect(good.unwrap, equals('Good result')));
  test('good unwrapOrDefault return val',
      () => expect(good.unwrapOrDefault('MyDefault'), 'Good result'));
  test('good unwrapOrElse returns value',
      () => expect(good.unwrapOrElse((e) => 'Hi!'), 'Good result'));

  // bad:
  test('bad.isOk return false', () => expect(bad.isOk, isFalse));
  test('bad.isErr return true', () => expect(bad.isErr, isTrue));
  test('bad is not Ok type', () => expect(bad, isNot(Ok)));
  test('bad is Err type', () => expect(bad, isA<Err>()));
  test('bad throws error while unwrap',
      () => expect(() => bad.unwrap, throwsArgumentError));
  test('bad val is null', () => expect(bad.val, null));
  test('bad err is Error', () => expect(bad.err, isA<Error>()));
  test('bad unwrapOrDefault return default',
      () => expect(bad.unwrapOrDefault('Bad value here'), 'Bad value here'));
  test('bad unwrapOrElse returns call result',
      () => expect(bad.unwrapOrElse((e) => 'Other val'), 'Other val'));

  test(
      'tryAsResultAsync returns Futire<Err> on exception',
      () async => expect(
          await tryAsResultAsync(
            () => File('notExistsFile').rename('newPath'),
          ),
          isA<Err>()));
  test(
      'tryAsResultAsync returns Future<Ok> on success',
      () async => expect(
          await tryAsResultAsync(
            () => Directory('.').list(),
          ),
          isA<Ok>()));

  test(
      'tryAsResultSync returns Futire<Err> on exception',
      () => expect(
          tryAsResultSync(
            () => File('notExistsFile').renameSync('newPath'),
          ),
          isA<Err>()));
  test(
      'tryAsResultSync returns Future<Ok> on success',
      () => expect(
          tryAsResultSync(
            () => Directory('.').listSync(),
          ),
          isA<Ok>()));

  test(
      'testResult return Ok on success',
      () => expect(
          testResult((Result r) => r.val > 2, Ok(3), 'not more then 2'),
          isA<Ok>()));

  test(
      '`test` method return Ok on success',
      () => expect(
          Ok(3).test((Result r) => r.val > 2, 'not more then 2'), isA<Ok>()));

  test(
      'testResult return Err on fail',
      () => expect(
          testResult((Result r) => r.val > 2, Ok(1), 'not more then 2'),
          isA<Err>()));

  test(
      'Result Err piping (operator)',
      () => expect(
          Err(ArgumentError('Bad value')).pipe((v) => v * 2), isA<Err>()));
  test(
      'Result Err piping (operator) cascade',
      () => expect(
          Err(ArgumentError('Bad value'))
              .pipe((v) => v * 2)
              .pipe((x) => x + 1)
              .err
              .toString(),
          equals(ArgumentError('Bad value').toString())));

  test('Result Ok piping (method)',
      () => expect(Ok(123).pipe((v) => v * 2), isA<Ok>()));
  test(
      'Result Ok piping cascade (method)',
      () => expect(
          Ok(123).pipe((v) => v * 2).pipe((x) => x + 1).unwrap, equals(247)));

  test(
      'Result Ok piping with Error prouces Err',
      () => expect(
          Ok(123).pipe((v) => v * 2).pipe((x) => AssertionError('my error')),
          isA<Err>()));

  test('Replace err with new value by replacePossibleErrorWith', () {
    var oldErr = Err(ArgumentError('Old'));
    var newErr = oldErr.replacePossibleErrorWith(ArgumentError('New'));
    expect(newErr.err.toString(), equals('Invalid argument(s): New'));
  });

  test('Replace err with new value by replacePossibleError', () {
    var oldErr = Err(ArgumentError('Old'))
        .replacePossibleError((old) => ArgumentError('New'));
    var newErr = oldErr.replacePossibleError((old) => ArgumentError('New'));
    expect(newErr.err.toString(), equals('Invalid argument(s): New'));
  });

  group('pipe() on Ok', () {
    test(' => val - Ok', () async {
      expect(Ok(123).pipe((n) => n + 1).unwrap, equals(124));
    });

    test(' => Ok - Ok', () async {
      expect(Ok(123).pipe((n) => Ok(n + 1)).unwrap, equals(124));
    });

    test(' => null - Err', () async {
      expect(Ok(123).pipe((n) => null).isErr, equals(true));
    });

    test(' => Error - Err', () async {
      expect(Ok(123).pipe((n) => ArgumentError('Bad Argument!')).isErr,
          equals(true));
    });

    test(' => Err - Err', () async {
      expect(Ok(123).pipe((n) => Err(ArgumentError('Bad Argument!'))).isErr,
          equals(true));
    });
  });

  group('ok() on Ok', () {
    test(' => val - Ok', () async {
      expect(Ok(123).ok((n) => n + 1).unwrap, equals(124));
    });

    test(' => Ok - Ok', () async {
      // do not auto unwrapping. How to solve this problem?:
      expect(Ok(123).ok((n) => Ok(n + 1)).unwrap, isA<Ok>());
    });
  });

  group('okAsync() on Ok', () {
    test(' => val - Ok', () async {
      var result = await Ok(123).okAsync((n) async => n + 1);
      expect(result.unwrap, equals(124));
    });
  });

  group('pipeAsync() on Ok', () {
    test(' => val - Ok', () async {
      var result = await Ok(123).pipeAsync((n) async => n + 1);
      expect(result.unwrap, equals(124));
    });

    test(' => Ok - Ok', () async {
      var result = await Ok(123).pipeAsync((n) async => Ok(n + 1));
      expect(result.unwrap, equals(124));
    });

    test(' => null - Err', () async {
      var result = await Ok(123).pipeAsync((n) async => null);
      expect(result.isErr, equals(true));
    });

    test(' => Error - Err', () async {
      var result =
          await Ok(123).pipeAsync((n) async => ArgumentError('Bad Argument!'));
      expect(result.isErr, equals(true));
    });

    test(' => Err - Err', () async {
      var result = await Ok(123)
          .pipeAsync((n) async => Err(ArgumentError('Bad Argument!')));
      expect(result.isErr, equals(true));
    });
  });

  group('ok() on Future<Ok>', () {
    test(' => val - Ok', () async {
      var result = await Future.value(Ok(123)).ok((n) => n + 1);
      expect(result.unwrap, equals(124));
    });
  });

  group('pipe() on Future<Ok>', () {
    test(' => val - Ok', () async {
      var result = await Future.value(Ok(123)).pipe((n) => n + 1);
      expect(result.unwrap, equals(124));
    });

    test(' => Ok - Ok', () async {
      var result = await Future.value(Ok(123)).pipe((n) => Ok(n + 1));
      expect(result.unwrap, equals(124));
    });

    test(' => null - Err', () async {
      var result = await Future.value(Ok(123)).pipe((n) => null);
      expect(result.isErr, equals(true));
    });

    test(' => Error - Err', () async {
      var result = await Future.value(Ok(123))
          .pipe((n) => ArgumentError('Bad Argument!'));
      expect(result.pipe((n) => ArgumentError('Bad Argument!')).isErr,
          equals(true));
    });

    test(' => Err - Err', () async {
      var result = await Future.value(Ok(123))
          .pipe((n) => Err(ArgumentError('Bad Argument!')));
      expect(result.isErr, equals(true));
    });
  });

  group('okAsync() on Future<Ok>', () {
    test(' => val - Ok', () async {
      var result = await Future.value(Ok(123)).okAsync((n) async => n + 1);
      expect(result.unwrap, equals(124));
    });
  });

  group('pipeAsync() on Future<Ok>', () {
    test(' => val - Ok', () async {
      var result = await Future.value(Ok(123)).pipeAsync((n) async => n + 1);
      expect(result.unwrap, equals(124));
    });

    test(' => Ok - Ok', () async {
      var result =
          await Future.value(Ok(123)).pipeAsync((n) async => Ok(n + 1));
      expect(result.unwrap, equals(124));
    });

    test(' => null - Err', () async {
      var result = await Future.value(Ok(123)).pipeAsync((n) async => null);
      expect(result.isErr, equals(true));
    });

    test(' => Error - Err', () async {
      var result = await Future.value(Ok(123))
          .pipeAsync((n) async => ArgumentError('Bad Argument!'));
      expect(result.isErr, equals(true));
    });

    test(' => Err - Err', () async {
      var result = await Future.value(Ok(123))
          .pipeAsync((n) async => Err(ArgumentError('Bad Argument!')));
      expect(result.isErr, equals(true));
    });
  });

  test('Pipe-like chained methods - Ok()', () async {
    var addr = Uri(scheme: 'https', host: 'vlang.io', path: '/utc_now');
    var client = HttpClient();
    var digitResult = await tryAsResultAsync(() => client.getUrl(addr))
        .pipeAsync((request) => request.close())
        .pipeAsync((response) => response.transform(utf8.decoder).join())
        .pipe((text) => int.tryParse(text))
        .pipe((number) => number + 2);

    expect(digitResult, isA<Ok>());
    expect(digitResult.unwrap, greaterThan(0));
  });

  test('Pipe-like chained methods - Err()', () async {
    var addr = Uri(scheme: 'https', host: 'vlang.io', path: '/bad_url');
    var client = HttpClient();
    var digitResult = await tryAsResultAsync(() => client.getUrl(addr))
        .pipeAsync((request) => request.close())
        .pipeAsync((response) => response.transform(utf8.decoder).join())
        .pipe((text) => int.tryParse(text))
        .pipe((number) => number + 2);

    expect(digitResult, isA<Err>());
    expect(digitResult.isErr, equals(true));
  });

  test('onResult sync', () {
    var sideEffect = 0;
    Err(AssertionError('my error')).onResult((r) => sideEffect += 1);
    expect(sideEffect, equals(1));

    Ok(123).onResult((r) => sideEffect += r.unwrap);
    expect(sideEffect, equals(124));
  });

  test('onOk sync', () {
    var sideEffect = 0;
    Err(AssertionError('my error')).onOk((r) => sideEffect += 1);
    expect(sideEffect, equals(0));

    Ok(123).onOk((v) => sideEffect += v);
    expect(sideEffect, equals(123));
  });

  test('onErr sync', () {
    var sideEffect = 0;
    Err(AssertionError('my error')).onErr((r) => sideEffect += 1);
    expect(sideEffect, equals(1));

    Ok(123).onErr((r) => sideEffect += 1);
    expect(sideEffect, equals(1));
  });
  test('onResult async', () async {
    var sideEffect = 0;
    await Future.value(Err(AssertionError('my error')))
        .onResult((r) => sideEffect += 1);
    expect(sideEffect, equals(1));

    await Future.value(Ok(123)).onResult((r) => sideEffect += r.unwrap);
    expect(sideEffect, equals(124));
  });

  test('onOk async', () async {
    var sideEffect = 0;
    await Future.value(Err(AssertionError('my error')))
        .onOk((r) => sideEffect += 1);
    expect(sideEffect, equals(0));

    await Future.value(Ok(123)).onOk((v) => sideEffect += v);
    expect(sideEffect, equals(123));
  });

  test('onErr async', () async {
    var sideEffect = 0;
    await Future.value(Err(AssertionError('my error')))
        .onErr((r) => sideEffect += 1);
    expect(sideEffect, equals(1));

    await Future.value(Ok(123)).onErr((r) => sideEffect += 1);
    expect(sideEffect, equals(1));
  });

  group('or', () {
    test('on Ok - not executed', () {
      var sideEffect = 0;
      Ok(123).or(() => sideEffect += 1);
      expect(sideEffect, equals(0));
    });

    test('on Err - executed', () {
      var sideEffect = 0;
      Err(ArgumentError('some message')).or(() => sideEffect += 1);
      expect(sideEffect, equals(1));
    });

    test('on Future<Ok> - not executed', () async {
      var sideEffect = 0;
      await Future.value(Ok(123)).or(() => sideEffect += 1);
      expect(sideEffect, equals(0));
    });

    test('on Err - executed', () async {
      var sideEffect = 0;
      await Future.value(Err(ArgumentError('some message')))
          .or(() => sideEffect += 1);
      expect(sideEffect, equals(1));
    });
  });

  group('orAsync', () {
    test('on Ok - not executed', () {
      var sideEffect = 0;
      Ok(123).orAsync(() => Future.value(sideEffect += 1));
      expect(sideEffect, equals(0));
    });

    test('on Err - executed', () {
      var sideEffect = 0;
      Err(ArgumentError('some message'))
          .orAsync(() => Future.value(sideEffect += 1));
      expect(sideEffect, equals(1));
    });

    test('on Future<Ok> - not executed', () async {
      var sideEffect = 0;
      await Future.value(Ok(123)).orAsync(() => Future.value(sideEffect += 1));
      expect(sideEffect, equals(0));
    });

    test('on Err - executed', () async {
      var sideEffect = 0;
      await Future.value(Err(ArgumentError('some message')))
          .orAsync(() => Future.value(sideEffect += 1));
      expect(sideEffect, equals(1));
    });
  });

}
