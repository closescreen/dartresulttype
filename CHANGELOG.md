
# Changelog

## 1.0.0

- Initial version, created by Stagehand

## 1.0.1

- Fix homepage

## 1.0.2

- Add example

## 1.0.3

- Change example

## 1.1.0

- tryAsResultSync, tryAsResultAsync, testResult - functions added

## 1.1.1

- example improved

## 1.2.0

- pipe operator for Result

## 1.2.1

- pipe function want T type

## 2.0.0

- Null safety was added

## 2.1.0

- Not need to use ! after unwrap

## 2.2.0

- replacePossibleErrorWith method added

## 2.2.1

- Fix errors

## 2.3.0

- Pipe operaton don't require null-aware operator

## 2.3.1

- Example was improved

## 2.4.0

- pipe() method added
- pipe() supports Error type
- documentation improved

## 2.4.1

-fix typo

## 2.4.2

- Fixed tryAsResultSync

## 2.5.0

- Pipe on Future<Result> was added

## 2.5.1

- Sample fixed

## 2.5.2

- Sample improved

## 2.5.3

- Tests improved

## 2.6.0

- ok() and okAsync() added

## 2.7.0

Added methods: or, orAsync, onOk, onErr, onResult

## 2.7.1

Removed unused import from lib
